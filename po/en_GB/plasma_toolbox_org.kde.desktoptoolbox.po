# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:46+0000\n"
"PO-Revision-Date: 2022-01-01 16:28+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: contents/ui/ToolBoxContent.qml:269
#, kde-format
msgid "Choose Global Theme…"
msgstr "Choose Global Theme…"

#: contents/ui/ToolBoxContent.qml:276
#, kde-format
msgid "Configure Display Settings…"
msgstr "Configure Display Settings…"

#: contents/ui/ToolBoxContent.qml:298
#, kde-format
msgid "Exit Edit Mode"
msgstr "Exit Edit Mode"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Finish Customising Layout"

#~ msgid "Default"
#~ msgstr "Default"

#~ msgid "Desktop Toolbox"
#~ msgstr "Desktop Toolbox"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "Desktop Toolbox — %1 Activity"

#~ msgid "Lock Screen"
#~ msgstr "Lock Screen"

#~ msgid "Leave"
#~ msgstr "Leave"
