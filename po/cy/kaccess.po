# translation of kaccess.po to Cymraeg
# Copyright (C) 2003 Free Software Foundation, Inc.
# Kgyfieithu <kyfieithu@dotmon.com>, 2003
#
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2003-06-10 16:14+0100\n"
"Last-Translator: Kgyfieithu <kyfieithu@dotmon.com>\n"
"Language-Team: Cymraeg <cy@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KD ar ran KGyfieithu - meddalwedd rhydd yn Gymraeg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kyfieithu@dotmon.com"

#: kaccess.cpp:68
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:69
msgid "The Shift key is now active."
msgstr ""

#: kaccess.cpp:70
msgid "The Shift key is now inactive."
msgstr ""

#: kaccess.cpp:74
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:75
msgid "The Control key is now active."
msgstr ""

#: kaccess.cpp:76
msgid "The Control key is now inactive."
msgstr ""

#: kaccess.cpp:80
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:81
msgid "The Alt key is now active."
msgstr ""

#: kaccess.cpp:82
msgid "The Alt key is now inactive."
msgstr ""

#: kaccess.cpp:86
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:87
msgid "The Win key is now active."
msgstr ""

#: kaccess.cpp:88
msgid "The Win key is now inactive."
msgstr ""

#: kaccess.cpp:92
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:93
msgid "The Meta key is now active."
msgstr ""

#: kaccess.cpp:94
msgid "The Meta key is now inactive."
msgstr ""

#: kaccess.cpp:98
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:99
msgid "The Super key is now active."
msgstr ""

#: kaccess.cpp:100
msgid "The Super key is now inactive."
msgstr ""

#: kaccess.cpp:104
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:105
msgid "The Hyper key is now active."
msgstr ""

#: kaccess.cpp:106
msgid "The Hyper key is now inactive."
msgstr ""

#: kaccess.cpp:110
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr ""

#: kaccess.cpp:111
msgid "The Alt Graph key is now active."
msgstr ""

#: kaccess.cpp:112
msgid "The Alt Graph key is now inactive."
msgstr ""

#: kaccess.cpp:113
msgid "The Num Lock key has been activated."
msgstr ""

#: kaccess.cpp:113
msgid "The Num Lock key is now inactive."
msgstr ""

#: kaccess.cpp:114
msgid "The Caps Lock key has been activated."
msgstr ""

#: kaccess.cpp:114
msgid "The Caps Lock key is now inactive."
msgstr ""

#: kaccess.cpp:115
msgid "The Scroll Lock key has been activated."
msgstr ""

#: kaccess.cpp:115
msgid "The Scroll Lock key is now inactive."
msgstr ""

#: kaccess.cpp:331
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr ""

#: kaccess.cpp:333
#, fuzzy, kde-format
#| msgid "KDE Accessibility Tool"
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr "Erfyn Hygyrchedd KDE"

#: kaccess.cpp:619
#, kde-format
msgid "AltGraph"
msgstr ""

#: kaccess.cpp:621
#, kde-format
msgid "Hyper"
msgstr ""

#: kaccess.cpp:623
#, kde-format
msgid "Super"
msgstr ""

#: kaccess.cpp:625
#, kde-format
msgid "Meta"
msgstr "Meta"

#: kaccess.cpp:642
#, kde-format
msgid "Warning"
msgstr ""

#: kaccess.cpp:670
#, kde-format
msgid "&When a gesture was used:"
msgstr ""

#: kaccess.cpp:676
#, kde-format
msgid "Change Settings Without Asking"
msgstr ""

#: kaccess.cpp:677
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr ""

#: kaccess.cpp:678
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr ""

#: kaccess.cpp:721 kaccess.cpp:723
#, kde-format
msgid "Slow keys"
msgstr ""

#: kaccess.cpp:726 kaccess.cpp:728
#, kde-format
msgid "Bounce keys"
msgstr "Bysyll Adlam"

#: kaccess.cpp:731 kaccess.cpp:733
#, kde-format
msgid "Sticky keys"
msgstr ""

#: kaccess.cpp:736 kaccess.cpp:738
#, kde-format
msgid "Mouse keys"
msgstr ""

#: kaccess.cpp:745
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr ""

#: kaccess.cpp:748
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr ""

#: kaccess.cpp:752
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr ""

#: kaccess.cpp:755
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr ""

#: kaccess.cpp:766
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr ""

#: kaccess.cpp:769
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr ""

#: kaccess.cpp:772
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr ""

#: kaccess.cpp:778
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr ""

#: kaccess.cpp:789
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr ""

#: kaccess.cpp:792
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr ""

#: kaccess.cpp:798
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr ""

#: kaccess.cpp:809
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr ""

#: kaccess.cpp:812
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr ""

#: kaccess.cpp:821
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr ""

#: kaccess.cpp:830
#, kde-format
msgid "An application has requested to change this setting."
msgstr ""

#: kaccess.cpp:834
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""

#: kaccess.cpp:836
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""

#: kaccess.cpp:840
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr ""

#: kaccess.cpp:845
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""

#: kaccess.cpp:847
#, kde-format
msgid "An application has requested to change these settings."
msgstr ""

#: kaccess.cpp:852
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""

#: kaccess.cpp:873
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""

#: kaccess.cpp:875
#, kde-format
msgid "Slow keys has been disabled."
msgstr ""

#: kaccess.cpp:879
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""

#: kaccess.cpp:881
#, kde-format
msgid "Bounce keys has been disabled."
msgstr ""

#: kaccess.cpp:885
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""

#: kaccess.cpp:887
#, kde-format
msgid "Sticky keys has been disabled."
msgstr ""

#: kaccess.cpp:891
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""

#: kaccess.cpp:893
#, kde-format
msgid "Mouse keys has been disabled."
msgstr ""

#: main.cpp:49
#, fuzzy, kde-format
#| msgid "KDE Accessibility Tool"
msgid "Accessibility"
msgstr "Erfyn Hygyrchedd KDE"

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "(h) 2000, Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, fuzzy, kde-format
#| msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgid "Matthias Hoelzer-Kluepfel"
msgstr "(h) 2000, Matthias Hoelzer-Kluepfel"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "Awdur"

#~ msgid "KDE Accessibility Tool"
#~ msgstr "Erfyn Hygyrchedd KDE"

#~ msgid "kaccess"
#~ msgstr "kaccess"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Bounce Keys"
#~ msgstr "Bysyll Adlam"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Use bou&nce keys"
#~ msgstr "Bysyll Adlam"
