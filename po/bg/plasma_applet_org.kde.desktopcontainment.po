# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Zlatko Popov <zlatkopopov@fsa-bg.org>, 2008.
# Yasen Pramatarov <yasen@lindeas.com>, 2010, 2011.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_folderview\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-19 00:47+0000\n"
"PO-Revision-Date: 2022-08-17 10:35+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:16
#, kde-format
msgid "Location"
msgstr "Местоположение"

#: package/contents/config/config.qml:23
#: package/contents/ui/FolderViewLayer.qml:428
#, kde-format
msgid "Icons"
msgstr "Икони"

#: package/contents/config/config.qml:30
#, kde-format
msgid "Filter"
msgstr "Филтър"

#: package/contents/ui/BackButtonItem.qml:103
#, kde-format
msgid "Back"
msgstr "Назад"

#: package/contents/ui/ConfigFilter.qml:63
#, kde-format
msgid "Files:"
msgstr "Файлове:"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Show all"
msgstr "Показване на всички"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Show matching"
msgstr "Показване на съвпадение"

#: package/contents/ui/ConfigFilter.qml:64
#, kde-format
msgid "Hide matching"
msgstr "Скриване на съвпадението"

#: package/contents/ui/ConfigFilter.qml:69
#, kde-format
msgid "File name pattern:"
msgstr "Шаблон на име на файл:"

#: package/contents/ui/ConfigFilter.qml:76
#, kde-format
msgid "File types:"
msgstr "Типове файлове:"

#: package/contents/ui/ConfigFilter.qml:173
#, kde-format
msgid "File type"
msgstr "Тип файл"

#: package/contents/ui/ConfigFilter.qml:182
#, kde-format
msgid "Description"
msgstr "Описание"

#: package/contents/ui/ConfigFilter.qml:195
#, kde-format
msgid "Select All"
msgstr "Избиране Всички"

#: package/contents/ui/ConfigFilter.qml:205
#, kde-format
msgid "Deselect All"
msgstr "Размаркиране на всичко"

#: package/contents/ui/ConfigIcons.qml:63
#, kde-format
msgid "Panel button:"
msgstr "Бутон на панела:"

#: package/contents/ui/ConfigIcons.qml:69
#, kde-format
msgid "Use a custom icon"
msgstr "Използване на персонализирана икона"

#: package/contents/ui/ConfigIcons.qml:102
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Избиране…"

#: package/contents/ui/ConfigIcons.qml:108
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Изчистване на икона"

#: package/contents/ui/ConfigIcons.qml:128
#, kde-format
msgid "Arrangement:"
msgstr "Подреждане:"

#: package/contents/ui/ConfigIcons.qml:132
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Left to Right"
msgstr "От ляво надясно"

#: package/contents/ui/ConfigIcons.qml:133
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Right to Left"
msgstr "От дясно наляво"

#: package/contents/ui/ConfigIcons.qml:134
#, kde-format
msgctxt "@item:inlistbox arrangement of icons"
msgid "Top to Bottom"
msgstr "От горе надолу"

#: package/contents/ui/ConfigIcons.qml:143
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align left"
msgstr "Подравняване вляво"

#: package/contents/ui/ConfigIcons.qml:144
#, kde-format
msgctxt "@item:inlistbox alignment of icons"
msgid "Align right"
msgstr "Подравняване вдясно"

#: package/contents/ui/ConfigIcons.qml:159
#, kde-format
msgid "Lock in place"
msgstr "Заключване на място"

#: package/contents/ui/ConfigIcons.qml:173
#, kde-format
msgid "Sorting:"
msgstr "Сортиране:"

#: package/contents/ui/ConfigIcons.qml:181
#, kde-format
msgctxt "@item:inlistbox sort icons manually"
msgid "Manual"
msgstr "Ръчно"

#: package/contents/ui/ConfigIcons.qml:182
#, kde-format
msgctxt "@item:inlistbox sort icons by name"
msgid "Name"
msgstr "Име"

#: package/contents/ui/ConfigIcons.qml:183
#, kde-format
msgctxt "@item:inlistbox sort icons by size"
msgid "Size"
msgstr "Размер"

#: package/contents/ui/ConfigIcons.qml:184
#, kde-format
msgctxt "@item:inlistbox sort icons by file type"
msgid "Type"
msgstr "Вид"

#: package/contents/ui/ConfigIcons.qml:185
#, kde-format
msgctxt "@item:inlistbox sort icons by date"
msgid "Date"
msgstr "Дата"

#: package/contents/ui/ConfigIcons.qml:196
#, kde-format
msgctxt "@option:check sort icons in descending order"
msgid "Descending"
msgstr "Низходящ"

#: package/contents/ui/ConfigIcons.qml:204
#, kde-format
msgctxt "@option:check sort icons with folders first"
msgid "Folders first"
msgstr "Първо папки"

#: package/contents/ui/ConfigIcons.qml:218
#, kde-format
msgctxt "whether to use icon or list view"
msgid "View mode:"
msgstr "Режим на преглед:"

#: package/contents/ui/ConfigIcons.qml:220
#, kde-format
msgctxt "@item:inlistbox show icons in a list"
msgid "List"
msgstr "В списък"

#: package/contents/ui/ConfigIcons.qml:221
#, kde-format
msgctxt "@item:inlistbox show icons in a grid"
msgid "Grid"
msgstr "Решетка"

#: package/contents/ui/ConfigIcons.qml:232
#, kde-format
msgid "Icon size:"
msgstr "Размер на икона:"

#: package/contents/ui/ConfigIcons.qml:247
#, kde-format
msgctxt "@label:slider smallest icon size"
msgid "Small"
msgstr " Малък"

#: package/contents/ui/ConfigIcons.qml:256
#, kde-format
msgctxt "@label:slider largest icon size"
msgid "Large"
msgstr " Голям"

#: package/contents/ui/ConfigIcons.qml:265
#, kde-format
msgid "Label width:"
msgstr "Широчина на етикета:"

#: package/contents/ui/ConfigIcons.qml:268
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Narrow"
msgstr " Тесен"

#: package/contents/ui/ConfigIcons.qml:269
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Medium"
msgstr " Среден"

#: package/contents/ui/ConfigIcons.qml:270
#, kde-format
msgctxt "@item:inlistbox how long a text label should be"
msgid "Wide"
msgstr " Широк"

#: package/contents/ui/ConfigIcons.qml:278
#, kde-format
msgid "Text lines:"
msgstr "Редове на текст:"

#: package/contents/ui/ConfigIcons.qml:294
#, kde-format
msgid "When hovering over icons:"
msgstr "При посочване  на иконите:"

#: package/contents/ui/ConfigIcons.qml:296
#, kde-format
msgid "Show tooltips"
msgstr "Показване на подсказки"

#: package/contents/ui/ConfigIcons.qml:303
#, kde-format
msgid "Show selection markers"
msgstr " Маркери за избор"

#: package/contents/ui/ConfigIcons.qml:310
#, kde-format
msgid "Show folder preview popups"
msgstr "Изскачащи прозорци за визуализация на папки"

#: package/contents/ui/ConfigIcons.qml:320
#, kde-format
msgid "Rename:"
msgstr "Преименуване:"

#: package/contents/ui/ConfigIcons.qml:324
#, kde-format
msgid "Rename inline by clicking selected item's text"
msgstr "Директно преименуване чрез щракване на текст на избрания елемент"

#: package/contents/ui/ConfigIcons.qml:335
#, kde-format
msgid "Previews:"
msgstr "Преглед:"

#: package/contents/ui/ConfigIcons.qml:337
#, kde-format
msgid "Show preview thumbnails"
msgstr "Преглед с миниатюри"

#: package/contents/ui/ConfigIcons.qml:345
#, kde-format
msgid "Configure Preview Plugins…"
msgstr "Конфигуриране на приставки за визуализация…"

#: package/contents/ui/ConfigLocation.qml:81
#, kde-format
msgid "Show:"
msgstr "Показване:"

#: package/contents/ui/ConfigLocation.qml:83
#, kde-format
msgid "Desktop folder"
msgstr "Папка на работния плот"

#: package/contents/ui/ConfigLocation.qml:90
#, kde-format
msgid "Files linked to the current activity"
msgstr "Файлове, свързани с текущата дейност"

#: package/contents/ui/ConfigLocation.qml:97
#, kde-format
msgid "Places panel item:"
msgstr "Елемент от панела \"Места\":"

#: package/contents/ui/ConfigLocation.qml:130
#, kde-format
msgid "Custom location:"
msgstr "Персонализирано местоположение:"

#: package/contents/ui/ConfigLocation.qml:138
#, kde-format
msgid "Type path or URL…"
msgstr "Въвеждане на път или URL…"

#: package/contents/ui/ConfigLocation.qml:181
#, kde-format
msgid "Title:"
msgstr "Заглавие:"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "None"
msgstr "Без"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Default"
msgstr "По подразбиране"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Full path"
msgstr "Пълен път"

#: package/contents/ui/ConfigLocation.qml:183
#, kde-format
msgid "Custom title"
msgstr "Потребителско заглавие"

#: package/contents/ui/ConfigLocation.qml:198
#, kde-format
msgid "Enter custom title…"
msgstr "Въведете персонализирано заглавие…"

#: package/contents/ui/ConfigOverlay.qml:91
#, kde-format
msgid "Rotate"
msgstr "Ротация"

#: package/contents/ui/ConfigOverlay.qml:181
#, kde-format
msgid "Open Externally"
msgstr "Отваряне с външно приложение"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Hide Background"
msgstr "Скриване на фона"

#: package/contents/ui/ConfigOverlay.qml:193
#, kde-format
msgid "Show Background"
msgstr "Показване на фона"

#: package/contents/ui/ConfigOverlay.qml:241
#, kde-format
msgid "Remove"
msgstr "Премахване"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:24
#, kde-format
msgid "Preview Plugins"
msgstr "Визуализация на приставки"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:82
#, kde-format
msgid "OK"
msgstr "Добре"

#: package/contents/ui/FolderItemPreviewPluginsDialog.qml:95
#, kde-format
msgid "Cancel"
msgstr "Отказ"

#: package/contents/ui/main.qml:356
#, kde-format
msgid "Configure Desktop and Wallpaper…"
msgstr "Конфигуриране на работния плот и тапета…"

#: plugins/folder/directorypicker.cpp:32
#, kde-format
msgid "Select Folder"
msgstr "Избиране на папка"

#: plugins/folder/foldermodel.cpp:466
#, kde-format
msgid "&Refresh Desktop"
msgstr "Обнов&яване на работния плот"

#: plugins/folder/foldermodel.cpp:466 plugins/folder/foldermodel.cpp:1624
#, kde-format
msgid "&Refresh View"
msgstr "Оп&ресняване на изгледа"

#: plugins/folder/foldermodel.cpp:1633
#, kde-format
msgid "&Empty Trash"
msgstr "&Изпразване на кошчето"

#: plugins/folder/foldermodel.cpp:1636
#, kde-format
msgctxt "Restore from trash"
msgid "Restore"
msgstr "Възстановяване"

#: plugins/folder/foldermodel.cpp:1639
#, kde-format
msgid "&Open"
msgstr "&Отваряне"

#: plugins/folder/foldermodel.cpp:1754
#, kde-format
msgid "&Paste"
msgstr "&Поставяне"

#: plugins/folder/foldermodel.cpp:1871
#, kde-format
msgid "&Properties"
msgstr "&Свойства"

#: plugins/folder/viewpropertiesmenu.cpp:21
#, kde-format
msgid "Sort By"
msgstr "Сортиране по"

#: plugins/folder/viewpropertiesmenu.cpp:24
#, kde-format
msgctxt "@item:inmenu Sort icons manually"
msgid "Unsorted"
msgstr "Несортирано"

#: plugins/folder/viewpropertiesmenu.cpp:28
#, kde-format
msgctxt "@item:inmenu Sort icons by name"
msgid "Name"
msgstr "Име"

#: plugins/folder/viewpropertiesmenu.cpp:32
#, kde-format
msgctxt "@item:inmenu Sort icons by size"
msgid "Size"
msgstr "Размер"

#: plugins/folder/viewpropertiesmenu.cpp:36
#, kde-format
msgctxt "@item:inmenu Sort icons by file type"
msgid "Type"
msgstr "Вид"

#: plugins/folder/viewpropertiesmenu.cpp:40
#, kde-format
msgctxt "@item:inmenu Sort icons by date"
msgid "Date"
msgstr "Дата"

#: plugins/folder/viewpropertiesmenu.cpp:45
#, kde-format
msgctxt "@item:inmenu Sort icons in descending order"
msgid "Descending"
msgstr "Низходящ"

#: plugins/folder/viewpropertiesmenu.cpp:47
#, kde-format
msgctxt "@item:inmenu Sort icons with folders first"
msgid "Folders First"
msgstr "Първо папки"

#: plugins/folder/viewpropertiesmenu.cpp:50
#, kde-format
msgid "Icon Size"
msgstr "Размер на икона"

#: plugins/folder/viewpropertiesmenu.cpp:53
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Tiny"
msgstr "Миниатюрен"

#: plugins/folder/viewpropertiesmenu.cpp:54
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Very Small"
msgstr "Много малък"

#: plugins/folder/viewpropertiesmenu.cpp:55
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small"
msgstr "Малък"

#: plugins/folder/viewpropertiesmenu.cpp:56
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Small-Medium"
msgstr "Малък-среден"

#: plugins/folder/viewpropertiesmenu.cpp:57
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Medium"
msgstr "Среден"

#: plugins/folder/viewpropertiesmenu.cpp:58
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Large"
msgstr "Голям"

#: plugins/folder/viewpropertiesmenu.cpp:59
#, kde-format
msgctxt "@item:inmenu size of the icons"
msgid "Huge"
msgstr "Огромен"

#: plugins/folder/viewpropertiesmenu.cpp:67
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Arrange"
msgstr "Подреждане"

#: plugins/folder/viewpropertiesmenu.cpp:71
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Left to Right"
msgstr "От ляво надясно"

#: plugins/folder/viewpropertiesmenu.cpp:72
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Right to Left"
msgstr "От дясно наляво"

#: plugins/folder/viewpropertiesmenu.cpp:76
#, kde-format
msgctxt "@item:inmenu arrangement of icons"
msgid "Top to Bottom"
msgstr "От горе надолу"

#: plugins/folder/viewpropertiesmenu.cpp:81
#, kde-format
msgid "Align"
msgstr "Подравняване"

#: plugins/folder/viewpropertiesmenu.cpp:84
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Left"
msgstr "Вляво"

#: plugins/folder/viewpropertiesmenu.cpp:88
#, kde-format
msgctxt "@item:inmenu alignment of icons"
msgid "Right"
msgstr "Вдясно"

#: plugins/folder/viewpropertiesmenu.cpp:93
#, kde-format
msgid "Show Previews"
msgstr "Показване на предварителен преглед"

#: plugins/folder/viewpropertiesmenu.cpp:97
#, kde-format
msgctxt "@item:inmenu lock icon positions in place"
msgid "Locked"
msgstr "Заключени"
