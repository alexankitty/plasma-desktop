# translation of kaccess.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2007.
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kaccess\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-18 00:46+0000\n"
"PO-Revision-Date: 2021-08-21 16:25+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.0\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "रविशंकर श्रीवास्तव, जी. करूणाकर, राघवेंद्र कामत"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raviratlami@aol.in, raghu@raghukamath.com"

#: kaccess.cpp:68
msgid ""
"The Shift key has been locked and is now active for all of the following "
"keypresses."
msgstr "शिफ़्ट कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:69
msgid "The Shift key is now active."
msgstr "शिफ़्ट कुंजी अभी सक्रिय है"

#: kaccess.cpp:70
msgid "The Shift key is now inactive."
msgstr "शिफ़्ट कुंजी अभी अक्रिय है"

#: kaccess.cpp:74
msgid ""
"The Control key has been locked and is now active for all of the following "
"keypresses."
msgstr "कंट्रोल कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:75
msgid "The Control key is now active."
msgstr "कंट्रोल कुंजी अभी सक्रिय है"

#: kaccess.cpp:76
msgid "The Control key is now inactive."
msgstr "कंट्रोल कुंजी अभी अक्रिय है"

#: kaccess.cpp:80
msgid ""
"The Alt key has been locked and is now active for all of the following "
"keypresses."
msgstr "ऑल्ट कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:81
msgid "The Alt key is now active."
msgstr "ऑल्ट कुंजी अभी सक्रिय है"

#: kaccess.cpp:82
msgid "The Alt key is now inactive."
msgstr "ऑल्ट कुंजी अभी अक्रिय है"

#: kaccess.cpp:86
msgid ""
"The Win key has been locked and is now active for all of the following "
"keypresses."
msgstr "विन कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:87
msgid "The Win key is now active."
msgstr "विन कुंजी अभी सक्रिय है"

#: kaccess.cpp:88
msgid "The Win key is now inactive."
msgstr "विन कुंजी अभी अक्रिय है"

#: kaccess.cpp:92
msgid ""
"The Meta key has been locked and is now active for all of the following "
"keypresses."
msgstr "मेटा कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:93
msgid "The Meta key is now active."
msgstr "मेटा कुंजी अभी सक्रिय है"

#: kaccess.cpp:94
msgid "The Meta key is now inactive."
msgstr "मेटा कुंजी अभी अक्रिय है"

#: kaccess.cpp:98
msgid ""
"The Super key has been locked and is now active for all of the following "
"keypresses."
msgstr "सुपर कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:99
msgid "The Super key is now active."
msgstr "सुपर कुंजी अभी सक्रिय है"

#: kaccess.cpp:100
msgid "The Super key is now inactive."
msgstr "सुपर कुंजी अभी अक्रिय है"

#: kaccess.cpp:104
msgid ""
"The Hyper key has been locked and is now active for all of the following "
"keypresses."
msgstr "हायपर कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:105
msgid "The Hyper key is now active."
msgstr "हायपर कुंजी अभी सक्रिय है"

#: kaccess.cpp:106
msgid "The Hyper key is now inactive."
msgstr "हायपर कुंजी अभी अक्रिय है"

#: kaccess.cpp:110
msgid ""
"The Alt Graph key has been locked and is now active for all of the following "
"keypresses."
msgstr "ऑल्ट ग्राफ कुंजी ताला बंद है और अभी निम्न कीप्रेस हेतु सक्रिय रहेगा"

#: kaccess.cpp:111
msgid "The Alt Graph key is now active."
msgstr "ऑल्ट ग्राफ कुंजी अभी सक्रिय है"

#: kaccess.cpp:112
msgid "The Alt Graph key is now inactive."
msgstr "ऑल्ट ग्राफ कुंजी अभी अक्रिय है"

#: kaccess.cpp:113
msgid "The Num Lock key has been activated."
msgstr "न्यूम लॉक कुंजी सक्रिय किया जा चुका है"

#: kaccess.cpp:113
msgid "The Num Lock key is now inactive."
msgstr "न्यूम लॉक कुंजी अब अक्रिय है"

#: kaccess.cpp:114
msgid "The Caps Lock key has been activated."
msgstr "कैप्स लॉक कुंजी सक्रिय किया जा चुका है"

#: kaccess.cpp:114
msgid "The Caps Lock key is now inactive."
msgstr "कैप्स लॉक कुंजी अब अक्रिय है"

#: kaccess.cpp:115
msgid "The Scroll Lock key has been activated."
msgstr "स्क्रॉल लॉक कुंजी सक्रिय किया जा चुका है"

#: kaccess.cpp:115
msgid "The Scroll Lock key is now inactive."
msgstr "स्क्रॉल लॉक कुंजी अब अक्रिय है"

#: kaccess.cpp:331
#, kde-format
msgid "Toggle Screen Reader On and Off"
msgstr "स्क्रीन रीडर को चालू और बंद टॉगल करें"

#: kaccess.cpp:333
#, kde-format
msgctxt "Name for kaccess shortcuts category"
msgid "Accessibility"
msgstr "अभिगम्‍यता"

#: kaccess.cpp:619
#, kde-format
msgid "AltGraph"
msgstr "आल्ट-ग्राफ"

#: kaccess.cpp:621
#, kde-format
msgid "Hyper"
msgstr "हाइपर"

#: kaccess.cpp:623
#, kde-format
msgid "Super"
msgstr "सुपर"

#: kaccess.cpp:625
#, kde-format
msgid "Meta"
msgstr "मेटा"

#: kaccess.cpp:642
#, kde-format
msgid "Warning"
msgstr "चेतावनी"

#: kaccess.cpp:670
#, kde-format
msgid "&When a gesture was used:"
msgstr "जब एक मुख_मुद्रा इस्तेमाल किया जाए (&W) :"

#: kaccess.cpp:676
#, kde-format
msgid "Change Settings Without Asking"
msgstr "विन्यास में परिवर्तनों को बिना पूछे लागू करें"

#: kaccess.cpp:677
#, kde-format
msgid "Show This Confirmation Dialog"
msgstr "इस पुष्टिकरण संवाद को दिखाएँ"

#: kaccess.cpp:678
#, kde-format
msgid "Deactivate All AccessX Features & Gestures"
msgstr "सभी एक्सेसएक्स विशेषताओं व मुखमुद्राओं को अक्रिय करें"

#: kaccess.cpp:721 kaccess.cpp:723
#, kde-format
msgid "Slow keys"
msgstr "धीमी कुंजियाँ "

#: kaccess.cpp:726 kaccess.cpp:728
#, kde-format
msgid "Bounce keys"
msgstr "उछलती (बाउंस) कुंजियाँ"

#: kaccess.cpp:731 kaccess.cpp:733
#, kde-format
msgid "Sticky keys"
msgstr "चिपकती कुंजियाँ "

#: kaccess.cpp:736 kaccess.cpp:738
#, kde-format
msgid "Mouse keys"
msgstr "माउस कुंजियाँ"

#: kaccess.cpp:745
#, kde-format
msgid "Do you really want to deactivate \"%1\"?"
msgstr "क्या आप सचमुच \"%1\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:748
#, kde-format
msgid "Do you really want to deactivate \"%1\" and \"%2\"?"
msgstr "क्या आप सचमुच \"%1\" तथा \"%2\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:752
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\" and \"%3\"?"
msgstr "क्या आप सचमुच \"%1\", \"%2\" तथा \"%3\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:755
#, kde-format
msgid "Do you really want to deactivate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "क्या आप सचमुच \"%1\", \"%2\", \"%3\" तथा \"%4\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:766
#, kde-format
msgid "Do you really want to activate \"%1\"?"
msgstr "क्या आप सचमुच \"%1\" को सक्रिय करना चाहते हैं?"

#: kaccess.cpp:769
#, kde-format
msgid "Do you really want to activate \"%1\" and to deactivate \"%2\"?"
msgstr "क्या आप सचमुच \"%1\" को सक्रिय तथा \"%2\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:772
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\" and \"%3\"?"
msgstr ""
"क्या आप सचमुच \"%1\" को सक्रिय तथा  \"%2\" तथा \"%3\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:778
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and to deactivate \"%2\", \"%3\" and "
"\"%4\"?"
msgstr ""
"क्या आप सचमुच \"%1\" को सक्रिय तथा \"%2\", \"%3\" तथा \"%4\" को अक्रिय करना चाहते "
"हैं?"

#: kaccess.cpp:789
#, kde-format
msgid "Do you really want to activate \"%1\" and \"%2\"?"
msgstr "क्या आप सचमुच \"%1\" तथा \"%2\" को सक्रिय करना चाहते हैं?"

#: kaccess.cpp:792
#, kde-format
msgid ""
"Do you really want to activate \"%1\" and \"%2\" and to deactivate \"%3\"?"
msgstr ""
"क्या आप सचमुच \"%1\" तथा \"%2\" को सक्रिय हैं तथा \"%3\" को अक्रिय करना चाहते हैं?"

#: kaccess.cpp:798
#, kde-format
msgid ""
"Do you really want to activate \"%1\", and \"%2\" and to deactivate \"%3\" "
"and \"%4\"?"
msgstr ""
"क्या आप सचमुच \"%1\", तथा \"%2\" को सक्रिय तथा \"%3\" तथा \"%4\" को अक्रिय करना "
"चाहते हैं?"

#: kaccess.cpp:809
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\" and \"%3\"?"
msgstr "क्या आप सचमुच \"%1\", \"%2\" तथा \"%3\" को सक्रिय करना चाहते हैं?"

#: kaccess.cpp:812
#, kde-format
msgid ""
"Do you really want to activate \"%1\", \"%2\" and \"%3\" and to deactivate "
"\"%4\"?"
msgstr ""
"क्या आप सचमुच \"%1\", \"%2\" तथा \"%3\" को सक्रिय तथा \"%4\" को अक्रिय करना चाहते "
"हैं?"

#: kaccess.cpp:821
#, kde-format
msgid "Do you really want to activate \"%1\", \"%2\", \"%3\" and \"%4\"?"
msgstr "क्या आप सचमुच \"%1\", \"%2\", \"%3\" तथा \"%4\" को सक्रिय करना चाहते हैं?"

#: kaccess.cpp:830
#, kde-format
msgid "An application has requested to change this setting."
msgstr "एक अनुप्रयोग ने इस सेटिंग को बदलने के लिए निवेदन किया है."

#: kaccess.cpp:834
#, kde-format
msgid ""
"You held down the Shift key for 8 seconds or an application has requested to "
"change this setting."
msgstr ""
"या तो आपने शिफ़्ट कुंजी को ८ सेकंड के लिए दबाया हुआ था या किसी अनुप्रयोग ने इस सेटिंग को "
"बदलने के लिए पूछा है."

#: kaccess.cpp:836
#, kde-format
msgid ""
"You pressed the Shift key 5 consecutive times or an application has "
"requested to change this setting."
msgstr ""
"आपने शिफ़्ट कुंजी को लगातार ५ बार दबाया है या किसी अनुप्रयोग ने इस विन्यास को बदलने के "
"लिए निवेदन किया है."

#: kaccess.cpp:840
#, kde-format
msgid "You pressed %1 or an application has requested to change this setting."
msgstr "आपने %1 को दबाया या किसी अनुप्रयोग ने इस सेटिंग को बदलने के लिए पूछा है."

#: kaccess.cpp:845
#, kde-format
msgid ""
"An application has requested to change these settings, or you used a "
"combination of several keyboard gestures."
msgstr ""
"किसी अनुप्रयोग ने इन सेटिंग को बदलने के लिए पूछा है या आपने बहुत से कुंजीपट मुखमुद्राओं के "
"संयोजन का इस्तेमाल किया है"

#: kaccess.cpp:847
#, kde-format
msgid "An application has requested to change these settings."
msgstr "किसी अनुप्रयोग ने इन विन्यासों को बदलने के लिए पूछा है."

#: kaccess.cpp:852
#, kde-format
msgid ""
"These AccessX settings are needed for some users with motion impairments and "
"can be configured in the KDE System Settings. You can also turn them on and "
"off with standardized keyboard gestures.\n"
"\n"
"If you do not need them, you can select \"Deactivate all AccessX features "
"and gestures\"."
msgstr ""
"ये एक्सेसएक्स सेटिंग्स कुछ गतिहीन उपयोगकर्ताओं के लिए आवश्यक हैं और इन्हें केडीई सिस्टम सेटिंग्स में "
"कॉन्फ़िगर किया जा सकता है। तुम भी पर और मानकीकृत कुंजीपटल इशारों के साथ बंद उन्हें बदल "
"सकते हैं।\n"
"\n"
"यदि आपको उनकी आवश्यकता नहीं है, तो आप \"सभी एक्सेसएक्स सुविधाओं और इशारों को निष्क्रिय "
"करें\" का चयन कर सकते हैं।"

#: kaccess.cpp:873
#, kde-format
msgid ""
"Slow keys has been enabled. From now on, you need to press each key for a "
"certain length of time before it gets accepted."
msgstr ""
"धीमी कुंजियाँ सक्षम की गई हैं. अब से आपको प्रत्येक कुंजी को एक निश्चित देर तक दबाना होगा "
"ताकि वो स्वीकारा जा सकें"

#: kaccess.cpp:875
#, kde-format
msgid "Slow keys has been disabled."
msgstr "धीमी कुंजियाँ अक्षम की गई हैं"

#: kaccess.cpp:879
#, kde-format
msgid ""
"Bounce keys has been enabled. From now on, each key will be blocked for a "
"certain length of time after it was used."
msgstr ""
"बाउंस कुंजियाँ सक्षम की गई हैं. अब से प्रत्येक कुंजी को उसके निश्चित इस्तेमाल की संख्या के बाद "
"अवरोधित कर दिया जाएगा."

#: kaccess.cpp:881
#, kde-format
msgid "Bounce keys has been disabled."
msgstr "बाउंस कुंजियाँ अक्षम की गई हैं"

#: kaccess.cpp:885
#, kde-format
msgid ""
"Sticky keys has been enabled. From now on, modifier keys will stay latched "
"after you have released them."
msgstr ""
"चिपकती कुंजियाँ सक्षम की गई हैं. अब से आपके द्वारा रिलीज किए जाने के बाद मॉडीफायर कुंजियां "
"लैच्ड रहेंगी."

#: kaccess.cpp:887
#, kde-format
msgid "Sticky keys has been disabled."
msgstr "चिपकती कुंजियाँ अक्षम की गई हैं"

#: kaccess.cpp:891
#, kde-format
msgid ""
"Mouse keys has been enabled. From now on, you can use the number pad of your "
"keyboard in order to control the mouse."
msgstr ""
"माउस कुंजियां सक्षम की गई हैं. अब से आप अपने कुंजीपट के नंबर पैड का इस्तेमाल माउस संकेतक को "
"नियंत्रित करने के लिए कर सकते हैं."

#: kaccess.cpp:893
#, kde-format
msgid "Mouse keys has been disabled."
msgstr "माउस कुंजियाँ अक्षम की गई हैं"

#: main.cpp:49
#, kde-format
msgid "Accessibility"
msgstr "अभिगम्‍यता"

#: main.cpp:49
#, kde-format
msgid "(c) 2000, Matthias Hoelzer-Kluepfel"
msgstr "(c) 2000, मैथियास होल्ज़र-क्लूपफेल"

#: main.cpp:51
#, kde-format
msgid "Matthias Hoelzer-Kluepfel"
msgstr "मैथियास होल्ज़र-क्लूपफेल"

#: main.cpp:51
#, kde-format
msgid "Author"
msgstr "लेखक"

#~ msgid "KDE Accessibility Tool"
#~ msgstr "केडीई पहुँच औज़ार"

#~ msgid "kaccess"
#~ msgstr "के-एक्सेस"

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Sticky Keys"
#~ msgstr "चिपकती कुंजियाँ "

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "Use &sticky keys"
#~ msgstr "चिपकती कुंजियाँ "

#, fuzzy
#~| msgid "Sticky keys"
#~ msgid "&Lock sticky keys"
#~ msgstr "चिपकती कुंजियाँ "

#, fuzzy
#~| msgid "Slow keys"
#~ msgid "&Use slow keys"
#~ msgstr "धीमी कुंजियाँ "

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Bounce Keys"
#~ msgstr "उछलती (बाउंस) कुंजियाँ"

#, fuzzy
#~| msgid "Bounce keys"
#~ msgid "Use bou&nce keys"
#~ msgstr "उछलती (बाउंस) कुंजियाँ"
